import { readFile } from "fs";

readFile(process.argv[2], function (err, fileData) {
    if (err === null) {
        let index = 0,
            line = null,
            timeEnd = null,
            entry = null,
            raw = 0;
        const fileString = fileData.toString(),
            lines = fileString.replace(/\r\n/g, "\n").split("\n"),
            lineLen = lines.length,
            store = {},
            output = [],
            // convert the provided date time format into ISO 8601 format and parse into a number for comparisons
            time = function (date, convert) {
                const str = (convert === true)
                    ? `${date.slice(0, 4)}-${date.slice(4, 6)}-${date.slice(6, 8)}T${date.slice(9)}Z`
                    : date;
                return [str, Date.parse(str)];
            };
        // provided data
        // <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
        // 0 = timestamp
        // 1 = satellite-id
        // 2 = red-high-limit
        // 3 = yellow-high-limit
        // 4 = yellow-low-limit
        // 5 = red-low-limit
        // 6 = raw-value
        // 7 = component
        //
        // output rule
        // BATT < red-low-limit
        // TSTAT > red-high-limit
        //
        // output format
        // {
        //     "satelliteId": 1234,
        //     "severity": "severity",
        //     "component": "component",
        //     "timestamp": "timestamp"
        // }
        do {
            line = lines[index].split("|");
            raw = Number(line[6]);
            // create a store based upon satellite name if not already created
            if (store[line[1]] === undefined) {
                store[line[1]] = {
                    BATT: [],
                    TSTAT: []
                };
            }

            // apply the rules
            if ((raw > Number(line[2]) && line[7] === "TSTAT") || (raw < Number(line[5]) && line[7] === "BATT")) {

                // each store should not exceed 3 entries as we are only concerned with 3 violations within the time interval
                if (store[line[1]][line[7]].length > 2) {
                    store[line[1]][line[7]].splice(0, 1);
                }

                // use the time parser created above
                timeEnd = time(line[0], true);

                // build out a data entry
                entry = {
                    "satelliteId": line[1],
                    "severity": (line[7] === "BATT")
                        ? "RED LOW"
                        : "RED HIGH",
                    "component": line[7],
                    "timestamp": timeEnd[0]
                };

                // push the data entry into the store
                store[line[1]][line[7]].push(entry);

                // if the store has three entries verify they are within the interval
                // if so then add the entry to the output array
                if (store[line[1]][line[7]].length > 2) {
                    if (timeEnd[1] - time(store[line[1]][line[7]][0].timestamp, false)[1] < 300000) {
                        output.push(store[line[1]][line[7]][0]);
                    }
                }
            }
            index = index + 1;
        } while (index < lineLen);
        process.stdout.write(JSON.stringify(output));
    } else {
        console.log("Error reading file!");
        process.stderr.write("Error reading file!");
        process.stderr.write(err);
        process.exit(1);
    }
});